import React, { createContext, useState } from 'react';
import { useLocalStorage } from './useLocalStorage';
const TodoContext = createContext();

function TodoProvider(props) {
   const {
      item: todos,
      saveItem: saveTodos,
      loading,
      error
   } = useLocalStorage('TODOS_V1', []);
   const [searchValue, setSearchValue] = useState('');
   const [openModal, setOpenModal] = useState(false);
 
   const completedTodos = todos.filter(todo => !!todo.completed).length;
   const totalTodos = todos.length;
   //Si los usuarios no escriben nada, este array sera el mismo que la lista de todos
   let searcherdTodos = [];
 
   if (!searchValue.length >= 1) {
     searcherdTodos = todos;
   } else {
     // Si los usuarios escriben aunque sea 1 letra en el input
     searcherdTodos = todos.filter(todo => {
       const todoText = todo.text.toLowerCase();
       const searchText = searchValue.toLowerCase();
       return todoText.includes(searchText);
     });
   }

   const addTodo = (text) => {
    const newTodos = [...todos];
    newTodos.push({
      completed: false,
      text
    })
    //Actualizar estado
    saveTodos(newTodos);
  }
 
   const completeTodo = (text) => {
     //Examina todo por todo caul tiene el mismo texto
     const todoIndex = todos.findIndex(todo => todo.text === text);
 
     const newTodos = [...todos];
     newTodos[todoIndex].completed = true;
     //Actualizar estado
     saveTodos(newTodos);
   }
 
   const delteTodo = (text) => {
     //Examina todo por todo caul tiene el mismo texto
     const todoIndex = todos.findIndex(todo => todo.text === text);
 
     const newTodos = [...todos];
     newTodos.splice(todoIndex, 1);
     //Actualizar estado
     saveTodos(newTodos);
   }
   return (
      <TodoContext.Provider value={{
        error,
        loading,
        totalTodos,
        completedTodos,
        searchValue,
        setSearchValue,
        searcherdTodos,
        addTodo,
        completeTodo,
        delteTodo,
        openModal,
        setOpenModal
      }}>
         {props.children}
      </TodoContext.Provider>
   )
}

export { TodoContext ,TodoProvider}