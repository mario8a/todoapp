import { useContext } from 'react';
import { TodoContext } from '../TodoContext/';

import { TodoCounter } from '../components/TodoCounter';
import { TodoSearch } from '../components/TodoSearch';
import { TodoList } from '../components/TodoList';
import { TodoItem } from '../components/TodoItem';
import { CreateTodoButton } from '../components/CreateTodoButton';
import { Modal } from '../Modal/';
import { TodoForm } from './../components/TodoForm/';

export const AppUI = () => {

   const {
      error, 
      loading, 
      searcherdTodos, 
      completeTodo, 
      delteTodo,
      openModal,
      setOpenModal
   } = useContext(TodoContext)

   return (
      <>
         <TodoCounter />
         <TodoSearch />

         <TodoList>
            { error && <p>Hubo un error</p> }
            { loading && <p>Cargando...</p> }
            { (!loading && !searcherdTodos.length) && <p>Crea tu primer TODO</p> }

            {searcherdTodos.map(todo => (
               <TodoItem 
                  key={todo.text} 
                  text={todo.text}
                  completed={todo.completed}
                  onComplete={() => completeTodo(todo.text)}
                  onDelete={() => delteTodo(todo.text)}
               />
            ))}
         </TodoList>

         { openModal && (
            <Modal>
               <TodoForm />
            </Modal>
         ) }

         <CreateTodoButton 
            setOpenModal={setOpenModal}
         />
      </>
   )
}