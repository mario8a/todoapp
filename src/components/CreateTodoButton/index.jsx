import React from 'react';
import './CreateTodoButton.css';

export const CreateTodoButton = (props) => {

   const onClickButton = () => {
      //Tambien se podia usar context, pero esta salio mejor :)
      props.setOpenModal(prevState => !prevState)
   }

   return (
      <button 
         className="CreateTodoButton"
         onClick={onClickButton}
      >
         +
      </button>
   )
}
